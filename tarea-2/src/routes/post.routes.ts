// path - ruta : api/posts

import { Router } from 'express';
import { listPosts, listPost, createPost, deletePost, updatePost } from "../controllers/post.controller";

const router = Router();

router.get("/", listPosts);
router.get("/:id", listPost);
router.post("/", createPost);
router.patch("/", updatePost);
router.delete("/:id", deletePost);

export default router;
