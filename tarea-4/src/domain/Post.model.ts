import mongoose from "mongoose";
import { DishI } from "./Dish.model";
import {UserI} from "./User.model";

export interface PostI extends mongoose.Document {
    title: string;
    content: string;
    status: boolean;
    user: UserI;
}
