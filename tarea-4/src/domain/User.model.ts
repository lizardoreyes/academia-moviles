import mongoose from "mongoose";

export interface UserI extends mongoose.Document {
    _id?: string;
    name: string;
    lastName: string;
    email: string;
    password: string;
    age?: number;
    address: AddressI;
    isSigned?: boolean;
}

export interface AddressI {
    name: string;
    country: string;
}
