import mongoose from "mongoose";
import { DishI } from "./Dish.model";

export interface OrderI extends mongoose.Document {
    dishes: DishI[],
    quantity: number[],
    client: string[]
}
