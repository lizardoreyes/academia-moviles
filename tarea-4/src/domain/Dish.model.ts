import mongoose from "mongoose";

export interface DishI extends mongoose.Document {
    _id?: string;
    picture: string;
    name: string;
    price: number;
    description: string;
    offert?: OffertI;
    isOfferted: boolean;
    delivered: number;
}

export interface OffertI {
    name: string;
    country: string;
}
