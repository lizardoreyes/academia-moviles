import Post from "../models/Post.model"
import { PostI } from "../domain/Post.model";

// LIST POSTS
const listPosts = async (req, res) => {
    try {
        const posts = await Post.find().populate('user') as PostI[];
        return res.status(200).send({
            success: true,
            posts
        });
    } catch (error) {
        res.status(500).send({
            error,
            success: false,
            message: "Ha ocurrido un problema"
        });
    }
}

// LIST POST
const listPost = async (req, res) => {
    const { id } = req.params

    try {
        const post = await Post.findById(id).populate('user') as PostI;
        return res.status(200).send({
            success: true,
            post
        });
    } catch (error) {
        res.status(500).send({
            error,
            success: false,
            message: "Ha ocurrido un problema"
        });
    }
}

// CREATE POST
const createPost = async (req, res) => {
    try {
        const { title, content, user_id } = req.body;
        if (title && content && user_id) {
            const post = new Post({ ...req.body, user: user_id}) as PostI;
            await post.save();

            return res.status(201).send({
                success: true,
                message: "Post creado correctamente."
            });
        }
        return res.status(400).send({
            success: false,
            message: "Datos invalidos"
        });
    } catch (error) {
        res.status(500).send({
            error,
            success: false,
            message: "Ha ocurrido un problema"
        });
    }
}

// UPDATE POST
const updatePost = async (req, res) => {
    try {
        const { _id, title, content } = req.body as PostI;
        if (title && content) {
            await Post.findByIdAndUpdate(_id, req.body);

            return res.status(201).send({
                success: true,
                message: "Post actualizado correctamente."
            });
        }
        return res.status(400).send({
            success: false,
            message: "Datos invalidos"
        });
    } catch (error) {
        res.status(500).send({
            error,
            success: false,
            message: "Ha ocurrido un problema"
        });
    }
}

// DELETE POST
const deletePost = async (req, res) => {
    try {
        const { id } = req.params;

        if (id) {
            await Post.findByIdAndRemove(id);

            return res.status(201).send({
                success: true,
                message: "Post eliminado correctamente."
            });
        }
        return res.status(400).send({
            success: false,
            message: "Datos invalidos"
        });
    } catch (error) {
        res.status(500).send({
            error,
            success: false,
            message: "Ha ocurrido un problema"
        });
    }
}

export { listPosts, listPost, createPost, updatePost, deletePost };
