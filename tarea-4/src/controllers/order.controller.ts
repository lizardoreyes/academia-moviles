import { Response, Request } from "express";
import Order from "../models/Order.model";
import { networkError, networkSuccess, serverError } from "../middlewares/response.middleware";
import { OrderI } from "../domain/Order.model";
import {DishI} from "../domain/Dish.model";

const createOrder = async (req: Request, res: Response) => {

    try {
        const order = new Order(req.body) as OrderI;
        await order.save();

        networkSuccess(res, order, "Orden creado correctamente", 201)

    } catch (error) {
        serverError(res, error)
    }
}

const getAll = async (req: Request, res: Response) => {

    try {
        const orders = await Order.find().populate('dishes').populate('client', 'name') as OrderI[];

        networkSuccess(res, orders, "Lista de ordenes")

    } catch (error) {
        serverError(res, error)
    }
}

const getByClientId = async (req: Request, res: Response) => {

    try {
        const clientId = req.params.id
        const ordersByClient = await Order.find({ client: clientId }).populate('dishes') as OrderI[];

        const ordersMapped = ordersByClient.map((order: OrderI) => {
            const dishes = order.dishes.map((dish: DishI) => {
                const { isOfferted, picture, price, name, _id, description } = dish
                return { isOfferted, picture, price, name, _id, description }
            })

            return {
                dishes,
                id: order._id,
                quantity: order.quantity
            }
        })

        networkSuccess(res, ordersMapped, "Lista de ordenes del cliente")

    } catch (error) {
        serverError(res, error)
    }
}

export { createOrder, getAll, getByClientId }
