import { Response, Request } from "express";
import Dish from "../models/Dish.model";
import DishDeleted from "../database/Logic/DishDeleted.model"
import {networkError, networkSuccess, serverError} from "../middlewares/response.middleware";
import { DishI } from "../domain/Dish.model";

// LIST DISHES
const getAll = async (req: Request, res: Response) => {

    try {
        const dishes = await Dish.find() as DishI[];

        networkSuccess(res, dishes, "Listado de platos")

    } catch (error) {
        serverError(res, error)
    }
}

// LIST DISH
const getById = async (req: Request, res: Response) => {

    try {
        const dish = await Dish.findById(req.params.id) as DishI;
        if(dish) {
            return networkSuccess(res, dish, "Plato encontrado")
        }

        networkError(res, "Plato no encontrado", "No se ha encontrado el plato")

    } catch (error) {
        serverError(res, error)
    }
}

// CREATE DISH
const create = async (req: Request, res: Response) => {

    try {
        const dish = new Dish(req.body) as DishI;
        await dish.save();

        networkSuccess(res, dish, "Plato creado correctamente", 201)

    } catch (error) {
        serverError(res, error)
    }
}

// UPDATE DISH
const update = async (req: Request, res: Response) => {

    try {
        const dish = req.body as DishI
        await Dish.updateOne({_id: dish._id}, dish);

        networkSuccess(res, dish, "Plato actualizado correctamente", 201)

    } catch (error) {
        serverError(res, error)
    }
}

// DELETE DISH
// TODO: Añadir delete lógica
const deleteDish = async (req: Request, res: Response) => {
    const id = req.body._id;

    try {
        // Recuperamos el plato con el "id" enviado
        const dishDeleted = await Dish.findOneAndDelete({ _id: id }) as DishI;

        if(!dishDeleted) {
            return networkError(res, "Plato no encontrado", "No se ha encontrado el plato", 404)
        }

        // TODO: Crear Interface Dish
        const { _id, picture, name, price, description, offert, isOfferted, delivered } = dishDeleted as DishI;
        await DishDeleted.create({ picture, name, price, description, offert, isOfferted, delivered, _id });
        await Dish.deleteOne({ _id: id })

        networkSuccess(res, "Plato eliminado", "Plato eliminado correctamente", 201)

    } catch (error) {
        serverError(res, error)
    }
}


export { getAll, getById, create, update, deleteDish };
