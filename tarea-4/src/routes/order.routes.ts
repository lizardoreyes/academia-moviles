// path - ruta : api/orders
import { Router } from "express";
import { createOrder, getAll, getByClientId } from "../controllers/order.controller";
import { check } from "express-validator"
import { fieldValidator, idValidator } from "../middlewares/dish.middleware";

const router = Router();

router.get("/", getAll);
router.get("/client/:id", idValidator, getByClientId);
router.post("/", createOrder);

export default router;
