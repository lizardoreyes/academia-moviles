import { Schema, model } from "mongoose";

const DishDeletedSchema = new Schema({
    picture: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    offert: {
        type: Schema.Types.Map
    },
    isOfferted: {
        type: Boolean,
        default: false,
        required: true
    },
    delivered: {
        type: Schema.Types.Number,
        default: 0,
        required: true
    }
});

export default model("dishes-deleted", DishDeletedSchema);
