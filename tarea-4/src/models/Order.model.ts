import { Schema, model } from "mongoose";

const FoodOrderSchema = new Schema({
    dishes: [{
        type: Schema.Types.ObjectId,
        ref: 'dishes',
        required: true
    }],
    quantity: [{
        type: Number,
        required: true
    }],
    client: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    }
});

export default model("orders", FoodOrderSchema);
