import express from "express";
import cors from "cors";
import { config } from "dotenv";
import dbConnection from "./database/config";

import userRoutes from "./routes/user.routes";
import dishRoutes from "./routes/dish.routes";
import authRoutes from "./routes/auth.routes";
import orderRoutes from "./routes/order.routes";
import postRoutes from "./routes/post.routes";

config();
dbConnection();

const PORT: number = parseInt(process.env.PORT) | 3000;
const app = express();

// TODO: Añadir una colleccion más aparte de la usuario (model, controller, router)
// Añadido coleccion posts

/*
    TYPES:  number, boolean, string, null, any
*/

// let numeros: number[] = [1, 2, 3]
// let respuesta: any[] = [1, 2, "", {"key": "value"}]

app.use(cors());
app.use(express.json());

app.use("/api/users", userRoutes);
app.use("/api/dishes", dishRoutes);
app.use("/api/orders", orderRoutes);
app.use("/api/auth", authRoutes);
// TODO: hacer otro route con referencia
app.use("/api/posts", postRoutes);

app.listen(PORT, () => {
    console.log(`Servidor iniciado en el puerto ${PORT}`);
});
