import { Response } from "express";
import {NetworkResponseI} from "../domain/response.model";

const networkSuccess = (res: Response, data: any, message: string, status: number = 200) => {
    const response: NetworkResponseI = {
        message,
        data,
        success: true
    }

    res.status(status).send(response)
}

const networkError = (res: Response, error: any, message: string, status: number = 400) => {
    const response: NetworkResponseI = {
        message,
        error,
        success: false
    }

    res.status(status).send(response)
}

const serverError = (res: Response, error: any) => {
    const response: NetworkResponseI = {
        message: "Ha ocurrido un problema",
        error,
        success: false
    }

    res.status(500).send(response)
}

export { networkSuccess, networkError, serverError }
