import { validationResult } from 'express-validator';
import mongoose from "mongoose";

// Validar "campos enviados"
const fieldValidator = (req, res, next) => {
    const errors = validationResult(req);

    if(!errors.isEmpty()) {
        return res.status(400).send({
            success: false,
            code: 1,
            errors: errors.mapped(),
            message: "Faltan datos"
        });
    }

    next();
}

// Validar si el "ID" enviado es valido para mongodb
const idValidator = (req, res, next) => {

    // Recuperamos el "id" de los parametros, el body o la cadena de consulta
    const id = req.params._id || req.body._id || req.query._id || req.params.id || req.body.id || req.query.id

    if( !mongoose.Types.ObjectId.isValid(id) ) {
        return res.status(400).send({
            success: false,
            code: 2,
            errors: "ID no valido",
            message: "El id es invalido"
        });
    }

    next();
}


export { fieldValidator, idValidator }
