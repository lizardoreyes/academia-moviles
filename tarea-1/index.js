const express = require('express');
const path = require("path")
const app = express()
const PORT = 3000
const { BASE_URL, BASE_URL_V2, personasPATH } = require("./routes")

// Se ejecuta con "npm run dev" tambien

const personas = [
    {
        _id: 1,
        nombre: "Luis Alvarez",
        edad: 29,
        job: "Programador"
    },
    {
        _id: 2,
        nombre: "Lucas Ramirez",
        edad: 23,
        job: "Frontend"
    },
    {
        _id: 3,
        nombre: "Marcos Rojas",
        edad: 25,
        job: "Backend"
    }
]

const personas2 = [
    {
        _id: 1,
        nombre: "Luis Alvarez",
        edad: 29,
        job: "Programador",
        isSingle: false
    },
    {
        _id: 2,
        nombre: "Lucas Ramirez",
        edad: 23,
        job: "Frontend",
        isSingle: true
    },
    {
        _id: 3,
        nombre: "Marcos Rojas",
        edad: 25,
        job: "Backend",
        isSingle: true
    }
]

app.use(express.json())


// RETURN FILE HTML
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "index.html"), _ => {
        console.error("Hay un error");
    })
})

// LIST USERS V1
app.get(`${BASE_URL}${personasPATH}`, (_, res) => {
    res.send(personas)
})

// LIST USER V1
app.get(`${BASE_URL}${personasPATH}/:id`, (req, res) => {
    try {
        const { id } = req.params
        if(id) {
            const personaIndex = personas.findIndex(persona => persona._id == id)
            if(personaIndex != -1) {
                return res.send(personas[personaIndex])
            }
            return res.status(400).send({ message: "No se ha encontrado a la persona solicitada" })
        }
        res.status(403).send({ message: "El id es incorrecto", id })
    } catch (e) {
        res.status(500).send({ error: JSON.stringify(e) })
        return
    }
})

// CREATE USER V1
app.post(`${BASE_URL}${personasPATH}`, (req, res) => {
    try {
        const { nombre, edad, job } = req.body

        if(nombre && edad && job) {
            
            const nuevaPersona = {
                _id: personas.length + 1, 
                nombre, 
                edad, 
                job 
            }

            personas.push(nuevaPersona)
            return res.status(201).send("Persona agregada correctamente")
        }
        
        res.status(403).send({ message: "Datos incorrectos", ...nuevaPersona })
    } catch (e) {
        res.status(500).send({ error: JSON.stringify(e) })
        return
    }
})


// UPDATE USER V1
app.patch(`${BASE_URL}${personasPATH}`, (req, res) => {
    try {
        const persona = req.body

        if(persona._id) {
            const personaIndex = personas.findIndex(value => value._id == persona._id)
            if(personaIndex != -1) {
                personas[personaIndex] = { ...persona, _id: personas[personaIndex]._id }
                return res.status(201).send("Persona actualizada correctamente")
            }
            return res.status(400).send({ message: "No se ha encontrado coincidencias" })
        }
        res.status(403).send({ message: "Faltan campos" })
    } catch (e) {
        res.status(500).send({ error: JSON.stringify(e) })
        return
    }
})

// TODO: Implementar el metodo "DELETE"
// DELETE USER V1
app.delete(`${BASE_URL}${personasPATH}`, (req, res) => {
    try {
        const { _id } = req.body

        if(_id) {
            const personaIndex = personas.findIndex(value => value._id == _id)
            if(personaIndex != -1) {
                personas.splice(personaIndex, 1)
                return res.status(201).send("Persona eliminada correctamente")
            }
            return res.status(400).send({ message: "No se ha encontrado coincidencias" })
        }
        res.status(403).send({ message: "Faltan campos" })
    } catch (e) {
        res.status(500).send({ error: JSON.stringify(e) })
        return
    }
})

// LIST USERS V2
// Solo cambia la fuente de la información, en este caso
app.get(`${BASE_URL_V2}${personasPATH}`, (_, res) => {
    res.send(personas2)
})

app.listen(PORT, () => {
    console.log(`Servidor iniciado en el puerto ${PORT}`);
})