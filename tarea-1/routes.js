const BASE_URL = "/api"
const BASE_URL_V2 = "/api/v2"

const personasPATH = "/personas"

module.exports = {
    BASE_URL, 
    BASE_URL_V2, 
    personasPATH
}