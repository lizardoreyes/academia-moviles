// path - ruta : api/dishes
import { Router } from "express";
import { create, getAll, getById, update, deleteDish } from "../controllers/dish.controller";
import { check } from "express-validator"
import { fieldValidator, idValidator } from "../middlewares/dish.middleware";

const router = Router();

router.get("/", getAll);
router.get("/:id", idValidator, getById);
router.patch("/", idValidator, update);
router.delete("/", idValidator, deleteDish);

router.post("/", [
    check("picture", "El campo imagen es requerido").notEmpty(),
    check("name", "El campo nombre es requerido").notEmpty(),
    check("description", "El campo descripcion es requerido").notEmpty(),
    check("price", "El campo precio es requerido").notEmpty(),
    fieldValidator
], create);

export default router;
