import jwt from "jsonwebtoken";

const generateJWT = (uuid: string) => {
    return new Promise((resolve, reject) => {
        const payload = { uuid };

        jwt.sign(
            payload,
            process.env.JWT_KEY,
            { expiresIn: '24h'},
            (err, token) => {
            if (err) {
                reject({ message: "El token no pudo ser generado" });
                return;
            }

            resolve(token);
            return;
        });
    });
}

export default generateJWT;
