import bcrypt from 'bcryptjs';
import { Request, Response } from "express";
import User from '../models/User.model';
import { NetworkResponseI } from '../domain/response.model';
import generateJWT from "../utils/generateJWT.utils";

const login = async (req: Request, res: Response) => {
    let response: NetworkResponseI
    try {
        const { email, password } = req.body
        const userDb = await User.findOne({ email });

        if(!userDb) {
            response = {
                success: false,
                message: "Usuario no existe"
            }
            return res.status(404).send(response);
        }

        //@ts-ignore
        const validPassword = bcrypt.compareSync(password, userDb.password);
        if(!validPassword) {
            response = {
                success: false,
                message: "Usuario o contraseña incorrectos"
            }
            return res.status(400).send(response);
        }

        const token = await generateJWT(userDb._id);
        response = {
            message:"Usuario logeado correctamente",
            success: true,
            data: { token }
        }
        return res.send(response);
    } catch(error) {
        response = {
            success: false,
            message: "A ocurrido un problema"
        }
        return res.status(500).send(response);
    }
}

export { login }
