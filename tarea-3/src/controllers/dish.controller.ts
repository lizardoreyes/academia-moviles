import { Response, Request } from "express";
import Dish from "../models/Dish.model";
import DishDeleted from "../models/DishDeleted.model"
import { NetworkResponseI } from '../domain/response.model';

// LIST DISHES
const getAll = async (req: Request, res: Response) => {
    let response: NetworkResponseI;

    try {
        const dishes = await Dish.find();

        response = {
            success: true,
            data: dishes,
            message: "Listado de platos"
        };
        res.send(response);
    } catch (error) {
        response = {
            success: false,
            message: "Ha ocurrido un error",
            error
        }
        res.status(500).send(response);
    }
}

// LIST DISH
const getById = async (req: Request, res: Response) => {
    let response: NetworkResponseI;

    try {
        const dish = await Dish.findById(req.params.id);
        if(dish) {
            response = {
                success: true,
                data: dish,
                message: "Plato encontrado"
            };
            return res.send(response);
        }

        response = {
            success: false,
            message: "No se ha encontrado el plato",
            error: "Plato no encontrado"
        }
        return res.status(400).send(response);

    } catch (error) {
        response = {
            success: false,
            message: "Ha ocurrido un problema",
            error
        }
        res.status(500).send(response);
    }
}

// CREATE DISH
const create = async (req: Request, res: Response) => {
    let response: NetworkResponseI;

    try {
        const dish = new Dish(req.body);
        await dish.save();

        response = {
            success: true,
            data: dish,
            message: "Plato creado correctamente"
        };
        res.send(response).status(201);
    } catch (error) {
        response = {
            success: false,
            message: "Ha ocurrido un error",
            error
        }
        res.status(500).send(response);
    }
}

// UPDATE DISH
const update = async (req: Request, res: Response) => {
    let response: NetworkResponseI;

    try {
        const dish = req.body
        await Dish.updateOne({_id: dish._id}, dish);

        response = {
            success: true,
            data: dish,
            message: "Plato actualizado correctamente"
        };
        res.send(response).status(201);
    } catch (error) {
        response = {
            success: false,
            message: "Ha ocurrido un error",
            error
        }
        res.status(500).send(response);
    }
}

// DELETE DISH
// TODO: Añadir delete lógica
const deleteDish = async (req: Request, res: Response) => {
    let response: NetworkResponseI;

    try {
        // Recuperamos el plato con el "id" enviado
        const dish = await Dish.findById(req.body._id);

        // Eliminamos el id del "plato".
        // Se creara uno al momento de guardarlo
        //@ts-ignore
        const {_id, ...dishWithoutId} = dish._doc

        // Se crea el "plato eliminado" y se guarda en su tabla respectiva
        const dishDeleted = new DishDeleted(dishWithoutId)
        await dishDeleted.save();

        // Eliminamos el "plato"
        await dish.delete();

        response = {
            success: true,
            message: "Plato eliminado correctamente"
        };
        res.send(response).status(201);
    } catch (error) {
        response = {
            success: false,
            message: "Ha ocurrido un error",
            error
        }
        res.status(500).send(response);
    }
}


export { getAll, getById, create, update, deleteDish };
